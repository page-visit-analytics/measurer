module pages-visit-analytics

go 1.19

require (
	github.com/ClickHouse/clickhouse-go/v2 v2.6.1
	github.com/caarlos0/env/v7 v7.0.0
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/joho/godotenv v1.5.1
)

require (
	github.com/ClickHouse/ch-go v0.52.1 // indirect
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/go-faster/city v1.0.1 // indirect
	github.com/go-faster/errors v0.6.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/klauspost/compress v1.15.15 // indirect
	github.com/lib/pq v1.10.7 // indirect
	github.com/paulmach/orb v0.8.0 // indirect
	github.com/pierrec/lz4/v4 v4.1.17 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/segmentio/asm v1.2.0 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	go.opentelemetry.io/otel v1.13.0 // indirect
	go.opentelemetry.io/otel/trace v1.13.0 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
