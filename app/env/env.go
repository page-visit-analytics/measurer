package env

import (
	"github.com/joho/godotenv"
	"log"
)

var loaded = false

func Load() {
	if loaded {
		return
	}
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	loaded = true
}
