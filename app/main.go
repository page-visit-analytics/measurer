package main

import (
	"fmt"
	"log"
	"net/http"
	"pages-visit-analytics/db"
	"pages-visit-analytics/env"
	"pages-visit-analytics/operations"
	"strconv"
)

func main() {
	env.Load()
	_db := db.GetInstance()
	_db.Migrate()
	handle()
}

func handle() {
	http.HandleFunc("/add", handlerAdd)
	http.HandleFunc("/get", handlerGet)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
func handlerAdd(writer http.ResponseWriter, request *http.Request) {
	url := request.FormValue("url")
	id, err := strconv.Atoi(request.FormValue("id"))
	if err != nil {
		fmt.Print(err)
	}
	msg, err := operations.Add(id, url)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
	}

	_, err = writer.Write([]byte(msg))
	if err != nil {
		fmt.Print(err)
	}

}

func handlerGet(writer http.ResponseWriter, request *http.Request) {
	url := request.FormValue("url")
	id := request.FormValue("id")
	count := 5
	resp := fmt.Sprintf("user_id %s visits %s %d times", id, url, count)
	_, err := writer.Write([]byte(resp))
	if err != nil {
		fmt.Print(err)
	}
}
