package operations

import (
	"fmt"
	"pages-visit-analytics/db"
)

const query = `INSERT INTO app.measurements (user_id, url) values(?, ?)`

func Add(id int, url string) (string, error) {
	result, err := db.GetInstance().Connection.Exec(query, id, url)
	fmt.Println(result)

	msg := ""
	if err != nil {
		msg = err.Error()
	} else {
		msg = fmt.Sprintf("added %s for user_id %d", url, id)
	}

	return msg, err
}
