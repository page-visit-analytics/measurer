create table IF NOT EXISTS app.measurements
(
    timestamp DateTime('Europe/Moscow') DEFAULT now(),
    user_id   UInt16,
    url       String
) ENGINE = MergeTree() ORDER BY timestamp