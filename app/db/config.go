package db

import (
	"fmt"
	"github.com/caarlos0/env/v7"
)

var config *cfg

type cfg struct {
	Host     string `env:"DB_HOST,notEmpty"`
	Port     int    `env:"DB_PORT,notEmpty"`
	Name     string `env:"DB_NAME,notEmpty"`
	Username string `env:"DB_USERNAME,notEmpty"`
	Password string `env:"DB_PASSWORD" envDefault:""`
}

func (c *cfg) Load() {
	config = &cfg{}
	if err := env.Parse(config); err != nil {
		fmt.Printf("%+v\n", err)
	}
}
