package db

import (
	_ "database/sql"
	"fmt"
	_ "github.com/ClickHouse/clickhouse-go/v2"
	clickhouseClient "github.com/ClickHouse/clickhouse-go/v2"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/clickhouse"
	clickhouseMigrateDriver "github.com/golang-migrate/migrate/v4/database/clickhouse"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"time"
)
import "database/sql"

var db *DB

type Options struct {
	host            string
	port            int
	username        string
	password        string
	database        string
	migrationsTable string
	sourceURL       string
	driverName      string
}

type DB struct {
	Connection *sql.DB
}

var options *Options

func evaluateOptions() {
	config.Load()
	options = &Options{
		host:            config.Host,
		port:            config.Port,
		username:        config.Username,
		password:        config.Password,
		database:        config.Name,
		migrationsTable: "schema_migrations",
		sourceURL:       "file://migrations",
		driverName:      "clickhouse",
	}
}
func GetInstance() *DB {
	if db != nil {
		return db
	}
	evaluateOptions()
	db = &DB{}
	conn := clickhouseClient.OpenDB(clickhouseClientOptions())
	conn.SetConnMaxLifetime(time.Minute)
	conn.SetMaxOpenConns(100)
	db.Connection = conn

	return db
}

func (db *DB) Migrate() {
	driver, err := clickhouseMigrateDriver.WithInstance(db.Connection, &clickhouseMigrateDriver.Config{})
	if err != nil {
		fmt.Println(err)
		fmt.Println("clickhouse.WithInstance")
	}
	m, err := migrate.NewWithDatabaseInstance(
		options.sourceURL,
		options.database,
		driver,
	)
	if err != nil {
		fmt.Println(err)
		fmt.Println("migrate.NewWithDatabaseInstance")
	}
	err = m.Up()
	if err != nil {
		fmt.Println(err)
		fmt.Println("m.Up()")
	}
}

func clickhouseClientOptions() *clickhouseClient.Options {
	addr := fmt.Sprintf("%s:%d", options.host, options.port)
	return &clickhouseClient.Options{
		Addr: []string{addr},
		Auth: clickhouseClient.Auth{
			Database: options.database,
			Username: options.username,
			Password: options.password,
		},
		Settings: clickhouseClient.Settings{
			"max_execution_time": 60,
		},
		DialTimeout: time.Second * 30,
		Compression: &clickhouseClient.Compression{
			Method: clickhouseClient.CompressionLZ4,
		},
		Debug:                true,
		BlockBufferSize:      10,
		MaxCompressionBuffer: 10240,
	}
}
